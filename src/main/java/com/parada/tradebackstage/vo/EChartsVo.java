package com.parada.tradebackstage.vo;

import com.parada.tradebackstage.dto.Series;
import com.parada.tradebackstage.dto.XAxis;
import com.parada.tradebackstage.dto.YAxis;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * create by Leo 2020/07/20
 */
@Data
public class EChartsVo {
    @ApiModelProperty(value = "x轴")
    private XAxis xAxis;
    @ApiModelProperty(value = "y轴")
    private YAxis yAxis;
    @ApiModelProperty(value = "导入数据")
    private Series series;
}
