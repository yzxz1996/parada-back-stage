package com.parada.tradebackstage.vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;


public class ClothesCheckVo implements Serializable {

    @ApiModelProperty(value = "商品id")
    private String clothesId;

    @ApiModelProperty(value = "商品类别")
    private String clothesType;

    @ApiModelProperty(value = "商品名称")
    private String clothesName;

    @ApiModelProperty(value = "商品颜色")
    private String clothesColor;

    @ApiModelProperty(value = "商品尺寸")
    private String clothesSize;

    @ApiModelProperty(value = "库存")
    private Integer stock;

    @ApiModelProperty(value = "图片")
    private String img;

    @ApiModelProperty(value = "价格")
    private Double price;

    @ApiModelProperty(value = "细节")
    private String detail;

    @ApiModelProperty(value = "审核状态")
    private Integer checkStatus;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;


    public String getClothesId() {
        return clothesId;
    }


    public void setClothesId(String clothesId) {
        this.clothesId = clothesId;
    }


    public String getClothesType() {
        return clothesType;
    }


    public void setClothesType(String clothesType) {
        this.clothesType = clothesType;
    }


    public String getClothesName() {
        return clothesName;
    }


    public void setClothesName(String clothesName) {
        this.clothesName = clothesName;
    }


    public String getClothesColor() {
        return clothesColor;
    }


    public void setClothesColor(String clothesColor) {
        this.clothesColor = clothesColor;
    }


    public String getClothesSize() {
        return clothesSize;
    }


    public void setClothesSize(String clothesSize) {
        this.clothesSize = clothesSize;
    }


    public Integer getStock() {
        return stock;
    }


    public void setStock(Integer stock) {
        this.stock = stock;
    }


    public String getImg() {
        return img;
    }


    public void setImg(String img) {
        this.img = img;
    }



    public Double getPrice() {
        return price;
    }


    public void setPrice(Double price) {
        this.price = price;
    }


    public String getDetail() {
        return detail;
    }


    public void setDetail(String detail) {
        this.detail = detail;
    }


    public Integer getCheckStatus() {
        return checkStatus;
    }


    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }


    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}