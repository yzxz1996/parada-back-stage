package com.parada.tradebackstage.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * create by Leo 2020/07/20
 */
@Data
public class YAxis {
    @ApiModelProperty(value = "y轴坐标")
    private String type;
}
