package com.parada.tradebackstage.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * create by Leo 2020/07/20
 */
@Data
public class XAxis {
    @ApiModelProperty(value = "x轴坐标")
    private String type;
    @ApiModelProperty(value = "x轴数据")
    private List<String> data;
}
