package com.parada.tradebackstage.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * create by Leo 2020/07/20
 */
@Data
public class Series {
    @ApiModelProperty(value = "数据数值")
    private List<Integer> data;
    @ApiModelProperty(value = "数据类型")
    private String type;
}
