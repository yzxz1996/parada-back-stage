package com.parada.tradebackstage.dto;

import java.util.Date;


public class ClothesCheckDto {

    private String clothesId;


    private String clothesType;


    private String clothesName;


    private String clothesColor;


    private String clothesSize;


    private Integer stock;


    private String img;


    private Double price;


    private String detail;


    private Integer checkStatus;

    private Date createTime;


    private Date updateTime;


    public String getClothesId() {
        return clothesId;
    }


    public void setClothesId(String clothesId) {
        this.clothesId = clothesId;
    }


    public String getClothesType() {
        return clothesType;
    }


    public void setClothesType(String clothesType) {
        this.clothesType = clothesType;
    }

    public String getClothesName() {
        return clothesName;
    }


    public void setClothesName(String clothesName) {
        this.clothesName = clothesName;
    }


    public String getClothesColor() {
        return clothesColor;
    }


    public void setClothesColor(String clothesColor) {
        this.clothesColor = clothesColor;
    }


    public String getClothesSize() {
        return clothesSize;
    }


    public void setClothesSize(String clothesSize) {
        this.clothesSize = clothesSize;
    }


    public Integer getStock() {
        return stock;
    }


    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getImg() {
        return img;
    }


    public void setImg(String img) {
        this.img = img;
    }


    public Double getPrice() {
        return price;
    }


    public void setPrice(Double price) {
        this.price = price;
    }


    public String getDetail() {
        return detail;
    }


    public void setDetail(String detail) {
        this.detail = detail;
    }


    public Integer getCheckStatus() {
        return checkStatus;
    }


    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }


    public Date getCreateTime() {
        return createTime;
    }


    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public Date getUpdateTime() {
        return updateTime;
    }


    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}