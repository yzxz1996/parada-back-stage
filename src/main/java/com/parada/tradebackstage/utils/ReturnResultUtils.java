package com.parada.tradebackstage.utils;

/**
 * create by Leo 2020/06/17
 */
public class ReturnResultUtils {

    public static ReturnResult returnSuccess() {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(200);
        returnResult.setMsg("成功");
        return returnResult;
    }

    public static <T> ReturnResult returnSuccess(T data) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(200);
        returnResult.setMsg("成功");
        returnResult.setData(data);
        return returnResult;
    }

    public static ReturnResult returnFail(int code, String msg) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMsg(msg);
        return returnResult;

    }
}
