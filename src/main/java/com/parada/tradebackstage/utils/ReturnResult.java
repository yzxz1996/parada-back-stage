package com.parada.tradebackstage.utils;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * create by Leo 2020/06/17
 */
@Data
public class ReturnResult<T> {
    @ApiModelProperty("状态码")
    private int code;
    @ApiModelProperty("信息")
    private String msg;
    @ApiModelProperty("数据")
    private T data;
}
