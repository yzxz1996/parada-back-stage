package com.parada.tradebackstage.controller;

import com.parada.tradebackstage.service.AddClothesToCheckService;
import com.parada.tradebackstage.service.SysUserService;
import com.parada.tradebackstage.utils.ReturnResult;
import com.parada.tradebackstage.utils.ReturnResultUtils;
import com.parada.tradebackstage.vo.ClothesCheckVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @www.codesheep.cn 20190312
 */
@Api(tags = "后台登录相关")
@RestController
public class JwtAuthController {


    @Autowired
    private SysUserService sysUserService;

    // 登录
    @ApiOperation("登陆")
    @GetMapping(value = "/authentication/login")
    public ReturnResult createToken(@ApiParam(value = "用户名") @RequestParam String userName, @ApiParam(value = "密码") @RequestParam String password) throws AuthenticationException {
        return ReturnResultUtils.returnSuccess(sysUserService.jwtLogin(userName, password));
    }


}

