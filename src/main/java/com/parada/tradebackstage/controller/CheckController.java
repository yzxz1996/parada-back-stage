package com.parada.tradebackstage.controller;


import com.parada.tradebackstage.param.ClothesCheckParam;
import com.parada.tradebackstage.service.AddClothesToCheckService;
import com.parada.tradebackstage.service.AddReplyService;
import com.parada.tradebackstage.service.CheckService;
import com.parada.tradebackstage.service.EChartsService;
import com.parada.tradebackstage.utils.ReturnResult;
import com.parada.tradebackstage.utils.ReturnResultUtils;

import com.parada.tradebackstage.vo.CouponKindVo;

import com.parada.tradebackstage.vo.EChartsVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by yuhw on 2020/7/17
 */
@RestController
@RequestMapping(value = "/CheckClothesController")
public class CheckController {

    @Autowired
    private CheckService checkService;

    @Autowired
    private AddClothesToCheckService addClothesToCheckService;

    @Autowired
    private AddReplyService addReplyService;

    @Autowired
    private EChartsService EChartsService;

    @PreAuthorize("hasRole('ROLE_MERCHANT')")
    @ApiOperation(value = "提交审核商品")
    @GetMapping(value = "/toCheckClothes")
    public ReturnResult toCheckClothes(@Validated ClothesCheckParam clothesCheckParam) {
        if (addClothesToCheckService.addToCheck(clothesCheckParam)) {
            return ReturnResultUtils.returnSuccess("提交商品审核成功");
        } else {
            return ReturnResultUtils.returnFail(500, "提交审核失败，请仔细检查后重新提交");
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_MERCHANT')")
    @ApiOperation(value = "查看商品审核状态")
    @GetMapping(value = "/lookCheckStatus")
    public String lookCheckStatus(@RequestParam String clothesName) {
        if (addClothesToCheckService.lookCheckStatus(clothesName) == 0) {
            return "商品待审核";
        } else if (addClothesToCheckService.lookCheckStatus(clothesName) == 1) {
            return "商品已通过审核";
        } else {
            return "商品审核未通过";
        }
    }

    @PreAuthorize("hasRole('ROLE_MERCHANT')")
    @ApiOperation(value = "回复商品评论")
    @GetMapping(value = "/reply")
    public ReturnResult addReply(String clothesId, String userId, String reply) {
        if (addReplyService.addReply(clothesId, userId, reply) > 0) {
            return ReturnResultUtils.returnSuccess("回复成功！");
        } else {
            return ReturnResultUtils.returnFail(490, "回复失败！");
        }

    }

    @PreAuthorize("hasRole('ROLE_MERCHANT')")
    @ApiOperation(value = "商家发放优惠券")
    @GetMapping(value = "/addCoupon")
    public ReturnResult addCoupon(@Validated CouponKindVo couponKindVo) {
        if (addClothesToCheckService.addCoupon(couponKindVo) > 0) {
            return ReturnResultUtils.returnSuccess("发放成功");
        } else {
            return ReturnResultUtils.returnFail(401, "发放失败");
        }
    }

    //查看待审核 已审核驳回 的审核商品列表
    @GetMapping(value = "/queryAllClothesStatusList")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation("查看全部待审核列表")
    public ReturnResult queryAllClothesStatusList(int status) {

        return ReturnResultUtils.returnSuccess(checkService.queryAllClothesStatusList(status));
    }

//        //商品审核,这个方法是当上面queryAllClothesStatusList（）方法执行之后进入到
//        //未审核商品页面之后才有的，所以不需要带status，因为status为1或者2的商品的页面就没有这个方法
//        @GetMapping(value = "/querycheckClothes")
//        @PreAuthorize("hasRole('ROLE_ADMIN')")
//        @ApiOperation("输入商品ID查询需要审核的商品列表")
//        public ReturnResult<ClothesCheckVo> querycheckClothes (String clotheId){
//            return ReturnResultUtils.returnSuccess(checkService.querycheckClothes(clotheId));
//        }


    @GetMapping(value = "/checkClothes")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(value = "审核商品", notes = "审核通过将插入到商品数据库，并将status修改为1")
    public ReturnResult checkClothes(int status, String clotheId) {
        return ReturnResultUtils.returnSuccess(checkService.checkClothes(status, clotheId));

    }

    @ApiOperation(value = "返回ECharts数据")
    @PreAuthorize("hasRole('ROLE_MERCHANT')")
    @GetMapping(value = "ECharts")
    public ReturnResult<EChartsVo> ECharts(@RequestParam String clothesId, @RequestParam Integer day) {
        EChartsVo eChartsVo = EChartsService.ECharts(clothesId, day);
        return ReturnResultUtils.returnSuccess(eChartsVo);
    }
}



