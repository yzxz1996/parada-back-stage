package com.parada.tradebackstage.controller;

import com.parada.tradebackstage.service.BannerService;
import com.parada.tradebackstage.utils.ReturnResult;
import com.parada.tradebackstage.utils.ReturnResultUtils;
import com.parada.tradebackstage.vo.BannerVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * create by CuiGeGe 2020/07/20
 */
@RestController
@Api(tags = "添加banner")
@RequestMapping(value = "/BannerController")
public class BannerController {
    @Autowired
    private BannerService bannerService;


    @GetMapping(value = "/addBanner")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation("添加banner")
    public ReturnResult<BannerVo> addBanner(@Validated BannerVo bannerVo) {
        if (bannerService.addBanner(bannerVo)) {
            return ReturnResultUtils.returnSuccess();
        }
        return null;
    }
}
