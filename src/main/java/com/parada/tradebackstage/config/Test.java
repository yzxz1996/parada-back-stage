package com.parada.tradebackstage.config;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import springfox.documentation.service.ApiListing;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * create by Leo 2020/07/17
 */
public class Test {
    public static void main(String[] args) {
        SimpleDateFormat format = new SimpleDateFormat();
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, -7);
        long d = System.currentTimeMillis();
        System.out.println(d);
    }
}
