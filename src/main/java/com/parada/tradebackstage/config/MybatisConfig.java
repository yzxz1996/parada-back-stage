
package com.parada.tradebackstage.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * create by Leo 2020/06/16
 */
@Configuration
@MapperScan(basePackages = "com.parada.tradebackstage.mapper")
public class MybatisConfig {
}

