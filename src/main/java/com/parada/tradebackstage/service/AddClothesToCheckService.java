package com.parada.tradebackstage.service;

import com.parada.tradebackstage.param.ClothesCheckParam;
import com.parada.tradebackstage.vo.ClothesCheckVo;

import com.parada.tradebackstage.vo.CouponKindVo;
import org.springframework.validation.annotation.Validated;

/**
 * Created by yuhw on 2020/7/17
 */
public interface AddClothesToCheckService {
    //商家添加审核商品
    boolean addToCheck(ClothesCheckParam clothesCheckParam);

    //商家查看商品的审核状态
    int lookCheckStatus(String clothesName);

    //商家发放优惠券
    int addCoupon(CouponKindVo couponKindVo);
}
