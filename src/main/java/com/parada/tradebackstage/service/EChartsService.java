package com.parada.tradebackstage.service;

import com.parada.tradebackstage.dto.*;
import com.parada.tradebackstage.mapper.ClothesDetailsMapper;
import com.parada.tradebackstage.mapper.ClothesOrderMapper;
import com.parada.tradebackstage.vo.EChartsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * create by Leo 2020/07/20
 */

@Service
public class EChartsService {

    @Autowired
    private ClothesOrderMapper clothesOrderMapper;


    public EChartsVo ECharts(String clothesId, Integer day) {
        ClothesOrderExample clothesOrderExample = new ClothesOrderExample();
        clothesOrderExample.createCriteria().andClothesIdEqualTo(clothesId);
        List<ClothesOrderDto> clothesOrderDtos = clothesOrderMapper.selectByExample(clothesOrderExample);
        if (CollectionUtils.isEmpty(clothesOrderDtos)) {
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        XAxis xAxis = new XAxis();
        xAxis.setType("category");
        List<String> days = new ArrayList<>();
        List<Integer> sales = new ArrayList<>();
        Date date;
        switch (day) {
            case 1:
                calendar.add(Calendar.DATE, -1);
                date = calendar.getTime();
                Date finalDate = date;
                sales.add(clothesOrderDtos.stream().filter(p -> p.getCreateTime().compareTo(finalDate) > 0).mapToInt(x -> x.getNumber()).sum());
                days.add(format.format(date));
                calendar.add(Calendar.DATE, -1);
                date = calendar.getTime();
                Date finalDate1 = date;
                sales.add(clothesOrderDtos.stream().filter(p -> p.getCreateTime().compareTo(finalDate1) > 0 && p.getCreateTime().compareTo(finalDate) < 0).mapToInt(x -> x.getNumber()).sum());
                days.add(format.format(date));
                calendar.add(Calendar.DATE, -1);
                date = calendar.getTime();
                Date finalDate2 = date;
                sales.add(clothesOrderDtos.stream().filter(p -> p.getCreateTime().compareTo(finalDate2) > 0 && p.getCreateTime().compareTo(finalDate1) < 0).mapToInt(x -> x.getNumber()).sum());
                days.add(format.format(date));
                break;
            case 2:
                calendar.add(Calendar.MONTH, -1);
                date = calendar.getTime();
                Date finalDate3 = date;
                sales.add(clothesOrderDtos.stream().filter(p -> p.getCreateTime().compareTo(finalDate3) > 0).mapToInt(x -> x.getNumber()).sum());
                days.add(format.format(date));
                calendar.add(Calendar.MONTH, -1);
                date = calendar.getTime();
                Date finalDate4 = date;
                sales.add(clothesOrderDtos.stream().filter(p -> p.getCreateTime().compareTo(finalDate4) > 0 && p.getCreateTime().compareTo(finalDate3) < 0).mapToInt(x -> x.getNumber()).sum());
                days.add(format.format(date));
                calendar.add(Calendar.MONTH, -1);
                date = calendar.getTime();
                Date finalDate5 = date;
                sales.add(clothesOrderDtos.stream().filter(p -> p.getCreateTime().compareTo(finalDate5) > 0 && p.getCreateTime().compareTo(finalDate4) < 0).mapToInt(x -> x.getNumber()).sum());
                days.add(format.format(date));
                break;
            case 3:
                calendar.add(Calendar.YEAR, -1);
                date = calendar.getTime();
                Date finalDate6 = date;
                sales.add(clothesOrderDtos.stream().filter(p -> p.getCreateTime().compareTo(finalDate6) > 0).mapToInt(x -> x.getNumber()).sum());
                days.add(format.format(date));
                calendar.add(Calendar.YEAR, -1);
                date = calendar.getTime();
                Date finalDate7 = date;
                sales.add(clothesOrderDtos.stream().filter(p -> p.getCreateTime().compareTo(finalDate7) > 0 && p.getCreateTime().compareTo(finalDate6) < 0).mapToInt(x -> x.getNumber()).sum());
                days.add(format.format(date));
                calendar.add(Calendar.YEAR, -1);
                date = calendar.getTime();
                Date finalDate8 = date;
                sales.add(clothesOrderDtos.stream().filter(p -> p.getCreateTime().compareTo(finalDate8) > 0 && p.getCreateTime().compareTo(finalDate7) < 0).mapToInt(x -> x.getNumber()).sum());
                days.add(format.format(date));
                break;
            default:
                date = new Date();
        }
        xAxis.setData(days);
        YAxis yAxis = new YAxis();
        yAxis.setType("value");
        Series series = new Series();
        series.setType("line");
        series.setData(sales);
        EChartsVo eChartsVo = new EChartsVo();
        eChartsVo.setXAxis(xAxis);
        eChartsVo.setYAxis(yAxis);
        eChartsVo.setSeries(series);
        return eChartsVo;
    }
}
