package com.parada.tradebackstage.service;

/**
 * Created by yuhw on 2020/7/18
 */
public interface AddReplyService {

    int addReply(String clothesId, String userId, String reply);
}
