package com.parada.tradebackstage.service;

import com.parada.tradebackstage.dto.BannerDto;
import com.parada.tradebackstage.mapper.BannerMapper;
import com.parada.tradebackstage.vo.BannerVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * create by CuiGeGe 2020/07/20
 */
@Service
public class BannerService {
    @Autowired
    private BannerMapper bannerMapper;

    public boolean addBanner(BannerVo bannerVo) {
        BannerDto bannerDto = new BannerDto();
        BeanUtils.copyProperties(bannerVo, bannerDto);
        int num = bannerMapper.insertSelective(bannerDto);
        if (num > 0) {
            return true;
        }
        return false;
    }
}
