package com.parada.tradebackstage.service;

import com.parada.tradebackstage.dto.ClothesCheckDto;
import com.parada.tradebackstage.dto.ClothesCheckExample;

import com.parada.tradebackstage.dto.ClothesDetailsDto;
import com.parada.tradebackstage.mapper.ClothesCheckMapper;
import com.parada.tradebackstage.mapper.ClothesDetailsMapper;
import com.parada.tradebackstage.vo.ClothesCheckVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * create by CuiGeGe 2020/07/17
 */
@Service
public class CheckService {

    @Autowired
    private ClothesCheckMapper clothesCheckMapper;
    @Autowired
    private ClothesDetailsMapper clothesDetailsMapper;


    public List<ClothesCheckDto> queryAllClothesStatusList(int status) {
        ClothesCheckExample clothesCheckExample = new ClothesCheckExample();
        clothesCheckExample.createCriteria().andCheckStatusEqualTo(status);
        List<ClothesCheckDto> clothesCheckDtos = clothesCheckMapper.selectByExample(clothesCheckExample);
        return clothesCheckDtos;

    }

//    //先查出要被修改的商品列表
//    public List<ClothesCheckVo> querycheckClothes(String clotheId) {
//        ClothesCheckExample clothesCheckExample = new ClothesCheckExample();
//        clothesCheckExample.createCriteria().andClothesIdEqualTo(clotheId);
//        List<ClothesCheckDto> clothesCheckDtos = clothesCheckMapper.selectByExample(clothesCheckExample);
//        List<ClothesCheckVo> clothesCheckVos = new ArrayList<ClothesCheckVo>();
//        clothesCheckDtos.forEach(clothesCheckDto -> {
//            ClothesCheckVo clothesCheckVo = new ClothesCheckVo();
//            BeanUtils.copyProperties(clothesCheckDto, clothesCheckVo);
//            clothesCheckVos.add(clothesCheckVo);
//        });
//
//        return clothesCheckVos;
//    }


    public boolean checkClothes(int status, String clotheId) {
        ClothesCheckExample clothesCheckExample = new ClothesCheckExample();
        clothesCheckExample.createCriteria().andClothesIdEqualTo(clotheId);
        List<ClothesCheckDto> clothesCheckDtos = clothesCheckMapper.selectByExample(clothesCheckExample);
        clothesCheckDtos.get(0).setCheckStatus(status);
        int num = clothesCheckMapper.updateByExampleSelective(clothesCheckDtos.get(0), clothesCheckExample);
        if (num > 0) {
            ClothesDetailsDto clothesdetailsDto = new ClothesDetailsDto();
            ClothesCheckDto clothesCheckDto = clothesCheckDtos.get(0);
            clothesdetailsDto.setClothesColor(clothesCheckDto.getClothesColor());
            clothesdetailsDto.setClothesId(clothesCheckDto.getClothesId());
            clothesdetailsDto.setClothesName(clothesCheckDto.getClothesName());
            clothesdetailsDto.setClothesSize(clothesCheckDto.getClothesSize());
            clothesdetailsDto.setClothesType(clothesCheckDto.getClothesType());
            clothesdetailsDto.setDetail(clothesCheckDto.getDetail());
            clothesdetailsDto.setImg(clothesCheckDto.getImg());
            clothesdetailsDto.setPrice(clothesCheckDto.getPrice());
            clothesdetailsDto.setStock(clothesCheckDto.getStock());
            int nums = clothesDetailsMapper.insertSelective(clothesdetailsDto);
            if (nums > 0) {
                return true;
            }
        }
        return false;
    }
}
