package com.parada.tradebackstage.service;

import com.parada.tradebackstage.dto.*;
import com.parada.tradebackstage.mapper.SysRoleMapper;
import com.parada.tradebackstage.mapper.SysUserMapper;
import com.parada.tradebackstage.mapper.SysUserRoleMapper;
import com.parada.tradebackstage.utils.JwtTokenUtil;
import com.parada.tradebackstage.utils.RedisUtils;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by boot on 2020/6/19.
 */
@Service
public class SysUserService implements UserDetailsService {
    @Autowired
    HttpServletRequest httpServletRequest;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private SysUserMapper userMapper;
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;
    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        SysUserExample userExample = new SysUserExample();
        userExample.createCriteria().andNameEqualTo(name);
        //待优化 guava cache
        List<SysUser> sysUsers = userMapper.selectByExample(userExample);
        SysUser sysUser = new SysUser();
        if (!CollectionUtils.isEmpty(sysUsers)) {
            sysUser = sysUsers.get(0);
            SysUserRoleExample sysUserRoleExample = new SysUserRoleExample();
            sysUserRoleExample.createCriteria().andUserIdEqualTo(sysUser.getId());
            List<SysUserRole> sysUserRolesList = sysUserRoleMapper.selectByExample(sysUserRoleExample);
            List<SysRole> sysRoleList = new ArrayList<SysRole>();
            for (SysUserRole sysUserRoles : sysUserRolesList) {
                SysRoleExample sysRoleExample = new SysRoleExample();
                sysRoleExample.createCriteria().andIdEqualTo(sysUserRoles.getRoleId());
                sysRoleList.addAll(sysRoleMapper.selectByExample(sysRoleExample));
            }
            sysUser.setSysRoles(sysRoleList);
        }
        return sysUser;
    }

    public SysUser selectById(Integer id) {
        return userMapper.selectByPrimaryKey(id);
    }


    public String jwtLogin(String userName, String password) {
        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(userName, password);
        final Authentication authentication = authenticationManager.authenticate(upToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        final UserDetails userDetails = userDetailsService.loadUserByUsername(userName);
        final String token = jwtTokenUtil.generateToken(userDetails);
        return token;
    }

}
