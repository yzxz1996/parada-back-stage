package com.parada.tradebackstage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@MapperScan("com.parada.tradebackstage.mapper")
@SpringBootApplication
public class TradeBackStageApplication {

    public static void main(String[] args) {
        SpringApplication.run(TradeBackStageApplication.class, args);
    }

}
