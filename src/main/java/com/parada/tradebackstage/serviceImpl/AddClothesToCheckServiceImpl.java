package com.parada.tradebackstage.serviceImpl;

import com.parada.tradebackstage.dto.*;

import com.parada.tradebackstage.mapper.ClothesCheckMapper;
import com.parada.tradebackstage.mapper.ClothesKindMapper;

import com.parada.tradebackstage.mapper.CouponKindMapper;
import com.parada.tradebackstage.param.ClothesCheckParam;
import com.parada.tradebackstage.service.AddClothesToCheckService;
import com.parada.tradebackstage.vo.ClothesCheckVo;

import com.parada.tradebackstage.vo.CouponKindVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by yuhw on 2020/7/17
 */
@Service
public class AddClothesToCheckServiceImpl implements AddClothesToCheckService {

    @Autowired
    private ClothesCheckMapper clothescheckMapper;

    @Autowired
    private ClothesKindMapper clothesKindMapper;

    @Autowired
    private CouponKindMapper couponKindMapper;

    @Override
    public boolean addToCheck(ClothesCheckParam clothesCheckParam) {
        ClothesKindExample clotheskindExample = new ClothesKindExample();
        clotheskindExample.createCriteria().andClothesTypeEqualTo(clothesCheckParam.getClothesType());
        List<ClothesKindDto> clotheskindDtos = clothesKindMapper.selectByExample(clotheskindExample);
        ClothesCheckDto clothescheckDto = new ClothesCheckDto();
        clothescheckDto.setClothesId(clotheskindDtos.get(0).getSexId() + "_" + clothesCheckParam.getClothesType() + "_" + UUID.randomUUID().toString().replace("-", "").substring(0, 5));
        clothescheckDto.setClothesName(clothesCheckParam.getClothesName());
        clothescheckDto.setClothesType(clothesCheckParam.getClothesType());
        clothescheckDto.setClothesColor(clothesCheckParam.getClothesColor());
        clothescheckDto.setClothesSize(clothesCheckParam.getClothesSize());
        clothescheckDto.setStock(clothesCheckParam.getStock());
        clothescheckDto.setDetail(clothesCheckParam.getDetail());
        clothescheckDto.setImg(clothesCheckParam.getImg());
        clothescheckDto.setPrice(clothesCheckParam.getPrice());
        int i = clothescheckMapper.insertSelective(clothescheckDto);
        if (i > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int lookCheckStatus(String clothesName) {
        ClothesCheckExample clothescheckExample = new ClothesCheckExample();
        clothescheckExample.createCriteria().andClothesNameLike("%" + clothesName + "%");
        List<ClothesCheckDto> clothescheckDtos = clothescheckMapper.selectByExample(clothescheckExample);
        int checkStatus = clothescheckDtos.get(0).getCheckStatus();
        return checkStatus;
    }

    @Override
    public int addCoupon(CouponKindVo couponKindVo) {
        CouponKindDto couponKindDto = new CouponKindDto();
        couponKindDto.setClothesKind(couponKindVo.getClothesKind());
        couponKindDto.setCouponPrice(couponKindVo.getCouponPrice());
        couponKindDto.setCouponSubPrice(couponKindVo.getCouponSubPrice());
        couponKindDto.setValidate(couponKindVo.getValidate());
        return couponKindMapper.insertSelective(couponKindDto);
    }
}
