package com.parada.tradebackstage.serviceImpl;

import com.parada.tradebackstage.dto.ScoresDto;
import com.parada.tradebackstage.dto.ScoresExample;
import com.parada.tradebackstage.mapper.ScoresMapper;
import com.parada.tradebackstage.service.AddReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by yuhw on 2020/7/18
 */
@Service
public class AddReplyServiceImpl implements AddReplyService {


    @Autowired
    private ScoresMapper scoresMapper;

    @Override
    public int addReply(String clothesId, String userId, String reply) {
        ScoresExample scoresExample = new ScoresExample();
        scoresExample.createCriteria().andClothesIdEqualTo(clothesId).andUserIdEqualTo(userId);
        ScoresDto scoresDto = new ScoresDto();
        scoresDto.setReply(reply);
        return scoresMapper.updateByExampleSelective(scoresDto, scoresExample);
    }
}
